package chapter3.item10.inherience;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * @author jacka
 * @version 1.0 on 10/16/2021
 */
class ColorPointTest {

  @Disabled
  @Test
  void testSymmetry() {
    // First equals function violates symmetry (Page 42)
    Point      p  = new Point(1, 2);
    ColorPoint cp = new ColorPoint(1, 2, Color.RED);
    System.out.println(p.equals(cp) + " " + cp.equals(p));

    // Second equals function violates transitivity (Page 42)
    ColorPoint p1 = new ColorPoint(1, 2, Color.RED);
    Point      p2 = new Point(1, 2);
    ColorPoint p3 = new ColorPoint(1, 2, Color.BLUE);
    System.out.printf("%s %s %s%n",
        p1.equals(p2), p2.equals(p3), p1.equals(p3));

    // use the second equals, it will throw stack overflow
    final SmellPoint sp1 = new SmellPoint(1, 2, Smell.SMELL1);
    System.out.printf("%s %s", sp1.equals(p1), p1.equals(sp1));
  }
}