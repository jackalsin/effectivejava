package chapter2.item01.libForClient;

import chapter2.item01.serviceInterface.Service;
import chapter2.item01.serviceProviderInterface.Provider;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author jacka
 * @version 1.0 on 10/10/2021
 */
public class Services {
  private Services() {
  }

  private static final Map<String, Provider> providers = new ConcurrentHashMap<>();

  public static final String DEFAULT_PROVIDER_NAME = "<def>";

  public static void registerDefaultProvider(Provider p) {
    providers.put(DEFAULT_PROVIDER_NAME, p);
  }

  // ---- service access api -----------------
  // when the caller calls these apis, they don't have any dependency on those packages.
  public static Service newInstance() {
    return newInstance(DEFAULT_PROVIDER_NAME);
  }

  public static Service newInstance(String name) {
    Provider p = providers.get(name);
    if (p == null) {
      throw new IllegalArgumentException();
    }
    return p.newService();
  }
}
