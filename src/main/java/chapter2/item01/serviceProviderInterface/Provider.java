package chapter2.item01.serviceProviderInterface;

import chapter2.item01.serviceInterface.Service;

/**
 * @author jacka
 * @version 1.0 on 10/10/2021
 */
public interface Provider {
  Service newService();
}
