package chapter2.item02.pizza;

import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author jacka
 * @version 1.0 on 10/10/2021
 */
public abstract class Pizza {
  public enum Topping {HAM, MUSHROOM, ONION, PEPPER, SAUSAGE}

  final Set<Topping> toppings;

  // 这个技巧叫self-type idiom
  abstract static class Builder<T extends Builder<T>> {
    // noneof这样理解，另外有一个allof，区别是一个创建了空的set，一个是满的
    EnumSet<Topping> toppings = EnumSet.noneOf(Topping.class);

    public T addTopping(Topping topping) {
      toppings.add(Objects.requireNonNull(topping));
      return self();
    }

    abstract Pizza build();

    protected abstract T self();
  }

  Pizza(Builder<?> builder) {
    toppings = builder.toppings.clone();
  }

}
