package chapter2.item02.pizza;

import java.util.Objects;

/**
 * @author jacka
 * @version 1.0 on 10/10/2021
 */
public class NyPizza extends Pizza {
  public enum Size {SMALL, MEDIUM, LARGE}

  private final Size size;

  public static class Builder extends Pizza.Builder<Builder> {
    private final Size size;

    public Builder(Size size) {
      this.size = Objects.requireNonNull(size);
    }

    @Override
    public NyPizza build() {
      return new NyPizza(this);
    }

    @Override
    protected Builder self() {
      return this;
    }

  }

  NyPizza(Builder builder) {
    super(builder);
    size = builder.size;
  }
}
