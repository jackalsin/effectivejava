package chapter2.item02.pizza;

/**
 * @author jacka
 * @version 1.0 on 10/10/2021
 */
public class CarzonePizza extends Pizza {

  private final boolean sauceInPizza;

  private static class Builder extends Pizza.Builder<Builder> {
    private boolean sauceInPizza = false;

    // 此时的return type可以是child class
    public Builder sauceInPizza() {
      sauceInPizza = true;
      return this;
    }

    @Override
    Pizza build() {
      return new CarzonePizza(this);
    }

    @Override
    protected Builder self() {
      return this;
    }
  }

  CarzonePizza(Builder builder) {
    super(builder);
    sauceInPizza = builder.sauceInPizza;
  }
}
