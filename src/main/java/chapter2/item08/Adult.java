package chapter2.item08;

/**
 * @author jacka
 * @version 1.0 on 10/14/2021
 */
// Well-behaved client of resource with cleaner safety-net (Page 33)
public class Adult {
  public static void main(String[] args) {
    try (Room myRoom = new Room(7)) {
      System.out.println("Goodbye");
    }
  }
}
