package chapter2.item08;

/**
 * @author jacka
 * @version 1.0 on 10/14/2021
 */
// Ill-behaved client of resource with cleaner safety-net (Page 33)
public class Teenager {
  public static void main(String[] args) {
    new Room(99);
    System.out.println("Peace out");

    // Uncomment next line and retest behavior, but note that you MUST NOT depend on this behavior!
    System.gc();
  }
}
