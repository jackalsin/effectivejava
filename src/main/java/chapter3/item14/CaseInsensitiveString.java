package chapter3.item14;

import java.util.Objects;

/**
 * @author jacka
 * @version 1.0 on 10/17/2021
 */
public final class CaseInsensitiveString implements Comparable<CaseInsensitiveString> {
  private final String s;

  public CaseInsensitiveString(final String s) {
    this.s = Objects.requireNonNull(s);
  }

  // Fixed equals method (Page 40)
  @Override
  public boolean equals(Object o) {
    return o instanceof CaseInsensitiveString &&
        ((CaseInsensitiveString) o).s.equalsIgnoreCase(s);
  }

  @Override
  public int hashCode() {
    return s.hashCode();
  }

  @Override
  public String toString() {
    return s;
  }

  @Override
  public int compareTo(CaseInsensitiveString o) {
    return String.CASE_INSENSITIVE_ORDER.compare(s, o.s);
  }
}
