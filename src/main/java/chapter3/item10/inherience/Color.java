package chapter3.item10.inherience;

/**
 * @author jacka
 * @version 1.0 on 10/16/2021
 */
public enum Color {
  RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, VIOLET
}
