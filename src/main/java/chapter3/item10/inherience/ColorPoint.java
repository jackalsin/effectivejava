package chapter3.item10.inherience;

/**
 * @author jacka
 * @version 1.0 on 10/16/2021
 */
public class ColorPoint extends Point {
  private final Color color;

  public ColorPoint(int x, int y, final Color color) {
    super(x, y);
    this.color = color;
  }

  // Broken - violates symmetry!  (Page 41)
  @Override
  public boolean equals(final Object o) {
    if (!(o instanceof ColorPoint))
      return false;
    return super.equals(o) && ((ColorPoint) o).color == color;
  }

//  // Broken - violates transitivity! (page 42)
//  @Override
//  public boolean equals(Object o) {
//    if (!(o instanceof Point))
//      return false;
//
//    // If o is a normal Point, do a color-blind comparison
//    if (!(o instanceof ColorPoint))
//      return o.equals(this);
//
//    // o is a ColorPoint; do a full comparison
//    return super.equals(o) && ((ColorPoint) o).color == color;
//  }
}
