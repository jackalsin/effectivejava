package chapter3.item10.inherience;

/**
 * @author jacka
 * @version 1.0 on 10/16/2021
 */
public class Point {
  private final int x, y;

  public Point(int x, int y) {
    this.x = x;
    this.y = y;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Point)) {
      return false;
    }
    final Point p = (Point) o;
    return x == p.x && y == p.y;
  }
}
