package chapter3.item10.inherience;

/**
 * @author jacka
 * @version 1.0 on 10/16/2021
 */
public class SmellPoint extends Point {
  private final Smell smell;

  public SmellPoint(int x, int y, Smell smell) {
    super(x, y);
    this.smell = smell;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof SmellPoint)) return false;
    return super.equals(o) && ((SmellPoint) o).smell == smell;
  }

//  // Broken - violates transitivity! (page 42)
//  @Override
//  public boolean equals(Object o) {
//    if (!(o instanceof Point))
//      return false;
//
//    // If o is a normal Point, do a color-blind comparison
//    if (!(o instanceof SmellPoint))
//      return o.equals(this);
//
//    // o is a ColorPoint; do a full comparison
//    return super.equals(o) && ((SmellPoint) o).smell == smell;
//  }
}
